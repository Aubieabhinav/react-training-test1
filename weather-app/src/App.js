import './App.css';
import Login from './components/Login'
import Home from './pages/Home'
import { Routes, Route } from 'react-router-dom'
// import ForecastList from './ui/forecastList';

const App = () => {
  return (
    <div>
      <main>
        <Routes>
          <Route path='/' element={<Login />} />
          <Route path='/home' element={<Home />} />
          {/* unable to fetch the data so uncommented the route
           <Route path='/forecastList' element={<ForecastList />} /> */}
        </Routes>
      </main>
    </div>
  );
}

export default App;
