import React from 'react'
import { Formik, Form, Field } from 'formik'
import * as Yup from 'yup'
import { useNavigate } from 'react-router-dom'
import classes from './Login.module.css'

const SignupSchema = Yup.object().shape({
    email: Yup.string().email('Invalid email').required('Required'),
    password: Yup.string()
        .matches(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d@$!%*#?&]{8,12}$/,
            "Must Contain 8 Characters, One Uppercase, One Lowercase, One Number and one special case Character")
        .required('Required'),
})
const Login = () => {
    const navigate = useNavigate()
    return (
        <div>
            <h1>Login</h1>
            <Formik
                initialValues={{
                    email: 'Abc@gmail.com',
                    password: 'Password8'
                }}
                validationSchema={SignupSchema}
                onSubmit={values => {
                    localStorage.setItem("email", values.email)
                    localStorage.setItem("password", values.password)
                    navigate('/home')
                    console.log(values);
                }}
            >
                {({ errors, touched }) => (
                    <Form>
                        <label htmlFor="email">Email</label>
                        <div className={classes.email}>
                            <Field name="email" type="email" id="email" />
                            {errors.email && touched.email ? <div>{errors.email}</div> : null}
                        </div>
                        <label htmlFor="password">Password</label>
                        <div className={classes.password}>
                            <Field name="password" id="password" />
                            {errors.password && touched.password ? (
                                <div>{errors.password}</div>
                            ) : null}
                        </div>
                        <div className={classes}>
                            <button type="submit">Submit</button>
                        </div>
                    </Form>
                )}
            </Formik>
        </div>
    )
}
export default Login

