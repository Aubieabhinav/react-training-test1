// import axios from 'axios'
// import AuthContext from '../context/AuthContext'
import React, { useState } from 'react'
import { useNavigate } from 'react-router-dom'
import axios from 'axios'
import classes from './Home.module.css'

const apiKey = "2e85d1166e474ec0bf645425220909"

const Home = () => {

    const navigate = useNavigate()
    const [cityName, setCityName] = useState('')
    const [posts, setPosts] = useState([])

    const getWeatherForecastDataHandler = (e) => {
        e.preventDefault()
        axios.get(`https://api.weatherapi.com/v1/forecast.json?q=${cityName}&key=${apiKey}&day=4`)
            .then((res) => {
                setPosts(res.data)
                setCityName(e.target.value)
            })
            .catch(err => console.log(err))
    }

    const cityChangeHandler = (event) => {
        setCityName(event.target.value)
    }


    const logoutClickHandler = () => {
        navigate('/')
    }

    return (
        <>
            <nav className={classes.nav}>
                <button onClick={logoutClickHandler} className={classes.logoutBtn}>Logout</button>
                <form onSubmit={getWeatherForecastDataHandler}>
                    <input
                        type="text"
                        placeholder="Search name"
                        value={cityName}
                        onChange={cityChangeHandler}

                    />
                    <button type="submit">Get Forecast </button>
                </form>
                <div className={classes.userEmail}>
                    {localStorage.getItem('email')}
                </div>
            </nav>
            <div>
                {Object.values(posts).map(post =>
                    <h2 key={post.location}>
                        {post.current}
                    </h2>
                )}
                
            </div>
        </>
    )
}

export default Home